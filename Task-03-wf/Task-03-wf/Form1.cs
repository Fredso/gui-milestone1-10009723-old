﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Task_03_wf
{
    public partial class Form1 : Form
    {
        /********** UI LOGIC **********/
        public Form1()
        {
            InitializeComponent();
            lblq1.Text = q1;
            cbFruits.DataSource = Fruits;
        }

        private void cbFruits_SelectedIndexChanged(object sender, EventArgs e)
        {
            lblSelection.Text = DisplaySelection(cbFruits.SelectedIndex.ToString());
        }

        /********** PROGRAM LOGIC **********/

        static string q1 = "Please select a fruit:";
        static string[] Fruits = new string[4] { "apple", "pear", "banana", "feijoa" };

        static string DisplaySelection(string selectiontxt)
        {
            int selection;
            var answer = "";
            bool result = int.TryParse(selectiontxt, out selection);
            if (result & selection < Fruits.GetLength(0))
            {
                answer = $"The fruit selected is {Fruits[selection]}.";
            }
            else
            {
                answer = "That is not a valid option. Try again next time!";
            }
            return answer;
        }
    }
}
