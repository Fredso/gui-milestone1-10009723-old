﻿namespace Task_03_wf
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lblSelection = new System.Windows.Forms.Label();
            this.lblq1 = new System.Windows.Forms.Label();
            this.cbFruits = new System.Windows.Forms.ComboBox();
            this.SuspendLayout();
            // 
            // lblSelection
            // 
            this.lblSelection.AutoSize = true;
            this.lblSelection.Location = new System.Drawing.Point(61, 103);
            this.lblSelection.Name = "lblSelection";
            this.lblSelection.Size = new System.Drawing.Size(0, 13);
            this.lblSelection.TabIndex = 4;
            // 
            // lblq1
            // 
            this.lblq1.AutoSize = true;
            this.lblq1.Location = new System.Drawing.Point(25, 31);
            this.lblq1.Name = "lblq1";
            this.lblq1.Size = new System.Drawing.Size(29, 13);
            this.lblq1.TabIndex = 3;
            this.lblq1.Text = "lblq1";
            // 
            // cbFruits
            // 
            this.cbFruits.FormattingEnabled = true;
            this.cbFruits.Location = new System.Drawing.Point(141, 28);
            this.cbFruits.Name = "cbFruits";
            this.cbFruits.Size = new System.Drawing.Size(103, 21);
            this.cbFruits.TabIndex = 2;
            this.cbFruits.SelectedIndexChanged += new System.EventHandler(this.cbFruits_SelectedIndexChanged);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(284, 261);
            this.Controls.Add(this.lblSelection);
            this.Controls.Add(this.lblq1);
            this.Controls.Add(this.cbFruits);
            this.Name = "Form1";
            this.Text = "Selector";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lblSelection;
        private System.Windows.Forms.Label lblq1;
        private System.Windows.Forms.ComboBox cbFruits;
    }
}

