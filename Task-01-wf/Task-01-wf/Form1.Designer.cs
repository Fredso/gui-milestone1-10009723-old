﻿namespace Task_01_wf
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tbInitialAmount = new System.Windows.Forms.TextBox();
            this.lblq1 = new System.Windows.Forms.Label();
            this.lblResult = new System.Windows.Forms.Label();
            this.lblq2 = new System.Windows.Forms.Label();
            this.cmbConvType = new System.Windows.Forms.ComboBox();
            this.SuspendLayout();
            // 
            // tbInitialAmount
            // 
            this.tbInitialAmount.Location = new System.Drawing.Point(270, 24);
            this.tbInitialAmount.Name = "tbInitialAmount";
            this.tbInitialAmount.Size = new System.Drawing.Size(51, 20);
            this.tbInitialAmount.TabIndex = 11;
            // 
            // lblq1
            // 
            this.lblq1.AutoSize = true;
            this.lblq1.Location = new System.Drawing.Point(8, 27);
            this.lblq1.Name = "lblq1";
            this.lblq1.Size = new System.Drawing.Size(29, 13);
            this.lblq1.TabIndex = 6;
            this.lblq1.Text = "lblq1";
            // 
            // lblResult
            // 
            this.lblResult.AutoSize = true;
            this.lblResult.Location = new System.Drawing.Point(8, 90);
            this.lblResult.Name = "lblResult";
            this.lblResult.Size = new System.Drawing.Size(47, 13);
            this.lblResult.TabIndex = 8;
            this.lblResult.Text = "lblResult";
            // 
            // lblq2
            // 
            this.lblq2.AutoSize = true;
            this.lblq2.Location = new System.Drawing.Point(8, 59);
            this.lblq2.Name = "lblq2";
            this.lblq2.Size = new System.Drawing.Size(29, 13);
            this.lblq2.TabIndex = 12;
            this.lblq2.Text = "lblq2";
            // 
            // cmbConvType
            // 
            this.cmbConvType.FormattingEnabled = true;
            this.cmbConvType.Location = new System.Drawing.Point(270, 56);
            this.cmbConvType.Name = "cmbConvType";
            this.cmbConvType.Size = new System.Drawing.Size(51, 21);
            this.cmbConvType.TabIndex = 13;
            this.cmbConvType.SelectedIndexChanged += new System.EventHandler(this.cmbConvType_SelectedIndexChanged);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(340, 187);
            this.Controls.Add(this.cmbConvType);
            this.Controls.Add(this.lblq2);
            this.Controls.Add(this.tbInitialAmount);
            this.Controls.Add(this.lblq1);
            this.Controls.Add(this.lblResult);
            this.Name = "Form1";
            this.Text = "Distance Converter";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox tbInitialAmount;
        private System.Windows.Forms.Label lblq1;
        private System.Windows.Forms.Label lblResult;
        private System.Windows.Forms.Label lblq2;
        private System.Windows.Forms.ComboBox cmbConvType;
    }
}

