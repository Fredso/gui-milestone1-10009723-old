﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Task_01_wf
{
    public partial class Form1 : Form
    {
        /********** UI LOGIC **********/
        public Form1()
        {
            InitializeComponent();
            lblq1.Text = q1;
            lblq2.Text = q2;
            cmbConvType.DataSource = new List<string> { "Km", "Miles" };
            lblResult.Text = "";
        }

        private void cmbConvType_SelectedIndexChanged(object sender, EventArgs e)
        {
            lblResult.Text = ConvertDistance(cmbConvType.SelectedIndex.ToString(), tbInitialAmount.Text);
        }

        /********** PROGRAM LOGIC **********/

        static string q1 = "Please enter the distance to be converted:";
        static string q2 = "Please selec Km or Miles to convert given distance:";

        //It checks data integrity, converts the distance and returns the string result
        static string ConvertDistance(string ctxt, string dtxt)
        {
            double distance;
            int option;
            string answer = "";
            //It checks the introduced distance to be converted
            bool resultd = double.TryParse(dtxt, out distance);
            bool resultc = int.TryParse(ctxt, out option);
            if (resultc & resultd)
            {
                switch (option)
                {
                    //Conversion from Km to miles
                    case 0:
                        answer = $"The distance {distance} Km converted is {distance * 0.62137119} miles.";
                        break;
                    //Conversion from miles to Km
                    case 1:
                        answer = $"The distance {distance} miles converted is {distance * 1.609344} Km.";
                        break;
                    default:
                        answer = "That is not a valid option. Try again next time!";
                        break;
                }
            }
            else
            {
                answer = "The data introduced is not correct. Try again next time!";
            }
            return answer;
        }
    }
}
