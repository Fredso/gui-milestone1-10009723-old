﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task_05_console
{
    class Program
    {
        /********** UI LOGIC **********/
        static void Main(string[] args)
        {
            for (int i=1;i<=5;i++)
            {
                Console.Clear();
                Console.WriteLine(Result.q1);
                Console.WriteLine($"{Result.q2} {Result.Rounds + 1}.");
                Console.WriteLine($"{Result.q3} {Result.Score}.");
                Console.WriteLine($"{Result.q4}");
                Result result = new Result();
                result.CheckNumber(Console.ReadLine());
                Console.WriteLine(Result.Rmessage);
                Console.WriteLine("Please, press a key to continue");
                Console.ReadLine();
            }
            Console.WriteLine($"{Result.q2} {Result.Rounds}.");
            Console.WriteLine($"{Result.q3} {Result.Score}.");
            Console.WriteLine("Please, press enter to finish");
            Console.ReadLine();
        }
    }

    /********** PROGRAM LOGIC **********/

    class Result
    {
        public static int Rounds = 0;
        public static int Score = 0;
        public static string Rmessage;
        public static string q1 = "Try to discover the number. You have 5 attempts:";
        public static string q2 = "Your rounds:";
        public static string q3 = "Your Score:";
        public static string q4 = "Please, introduce a number from 1 to 5:";

        //Method to see if the introduced number is the same as the random one
        public void CheckNumber(string txtnumber)
        {
            int number;
            Random rnd = new Random();
            int rnumber = rnd.Next(1, 6);

            //It checks how many rounds has the user played
            Rounds++;
            if (Rounds <= 5)
            {
                //It checks the introduced numbers
                bool conversion = int.TryParse(txtnumber, out number);
                if (conversion & number > 0 & number < 6)
                {
                    //It checks if the number is right
                    if (number == rnumber)
                    {
                        Score++;
                        Rmessage = $"You did it! {rnumber} was the number.";
                    }
                    else
                    {
                        //result.Score = 1;
                        Rmessage = $"Nope. The number was {rnumber} Try again!";
                    }
                }
                else
                {
                    //result.Score = 1;
                    Rmessage = "Please, enter a proper number next time.";
                }
            }
            else
            {
                Rounds = 5;
                Rmessage = "Sorry, you reached 5 attempts." + '\n' +
                    "You cannot continue playing.";
            }
        }
    }
}
