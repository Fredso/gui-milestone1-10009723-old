﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task_02_console
{
    class Program
    {
        /********** UI LOGIC **********/
        static void Main(string[] args)
        {
            Console.WriteLine(q1);
            var Number1=Console.ReadLine();
            Console.WriteLine(q2);
            var Number2 = Console.ReadLine();

            var results = new string[3] { "", "", "" };
            results = Calculate(Number1, Number2);
            Console.WriteLine($"{s1} {results[0]}.");
            Console.WriteLine($"{s2} {results[1]}.");
            Console.WriteLine($"{s3} {results[2]}.");
            Console.WriteLine("Please, press enter to finish");
            Console.ReadLine();
        }

        /********** PROGRAM LOGIC **********/

        static string q1 = "Please introduce number 1:";
        static string q2 = "Please introduce number 2:";
        static string s1 = "Subtotal:";
        static string s2 = "GST:";
        static string s3 = "Total:";

        //It calculates the results
        static string[] Calculate(string N1, string N2)
        {
            var solutions = new string[3] { "", "", "" };
            double ND1, ND2;

            //It checks the introduced numbers
            bool result1 = double.TryParse(N1, out ND1);
            bool result2 = double.TryParse(N2, out ND2);
            if (result1 & result2)
            {
                solutions[0] = (ND1 + ND2).ToString("0.00");
                solutions[1] = ((ND1 + ND2) * 0.15).ToString("0.00");
                solutions[2] = ((ND1 + ND2) * 1.15).ToString("0.00");
            }
            else
            {
                solutions[0] = "Data not correct";
                solutions[1] = "Data not correct";
                solutions[2] = "Data not correct";
            }
            return solutions;
        }
    }
}
