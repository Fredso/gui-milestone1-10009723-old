﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

// The Blank Page item template is documented at http://go.microsoft.com/fwlink/?LinkId=402352&clcid=0x409

namespace Task_01_uwp
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class MainPage : Page
    {
        /********** UI LOGIC **********/
        public MainPage()
        {
            this.InitializeComponent();
            lblq1.Text = q1;
            lblq2.Text = q2;
            cmbConvType.ItemsSource = new List<string> { "Km", "Miles" };
            lblResult.Text = "";
        }

        private void cmbConvType_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            lblResult.Text = ConvertDistance(cmbConvType.SelectedIndex.ToString(), tbInitialAmount.Text);
        }

        /********** PROGRAM LOGIC **********/

        static string q1 = "Please enter the distance to be converted:";
        static string q2 = "Please selec Km or Miles to convert given distance:";

        //It checks data integrity, converts the distance and returns the string result
        static string ConvertDistance(string ctxt, string dtxt)
        {
            double distance;
            int option;
            string answer = "";
            //It checks the introduced distance to be converted
            bool resultd = double.TryParse(dtxt, out distance);
            bool resultc = int.TryParse(ctxt, out option);
            if (resultc & resultd)
            {
                switch (option)
                {
                    //Conversion from Km to miles
                    case 0:
                        answer = $"The distance {distance} Km converted is {distance * 0.62137119} miles.";
                        break;
                    //Conversion from miles to Km
                    case 1:
                        answer = $"The distance {distance} miles converted is {distance * 1.609344} Km.";
                        break;
                    default:
                        answer = "That is not a valid option. Try again next time!";
                        break;
                }
            }
            else
            {
                answer = "The data introduced is not correct. Try again next time!";
            }
            return answer;
        }

    }
}
