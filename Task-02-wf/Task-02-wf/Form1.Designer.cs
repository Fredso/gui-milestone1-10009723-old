﻿namespace Task_02_wf
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.cbnCalculate = new System.Windows.Forms.Button();
            this.tbxNumber1 = new System.Windows.Forms.TextBox();
            this.tbxNumber2 = new System.Windows.Forms.TextBox();
            this.lblq1 = new System.Windows.Forms.Label();
            this.lblq2 = new System.Windows.Forms.Label();
            this.lbls1 = new System.Windows.Forms.Label();
            this.lblSubtotal = new System.Windows.Forms.Label();
            this.lbls2 = new System.Windows.Forms.Label();
            this.lblGST = new System.Windows.Forms.Label();
            this.lblTotal = new System.Windows.Forms.Label();
            this.lbls3 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // cbnCalculate
            // 
            this.cbnCalculate.Location = new System.Drawing.Point(200, 220);
            this.cbnCalculate.Name = "cbnCalculate";
            this.cbnCalculate.Size = new System.Drawing.Size(75, 23);
            this.cbnCalculate.TabIndex = 0;
            this.cbnCalculate.Text = "Calculate";
            this.cbnCalculate.UseVisualStyleBackColor = true;
            this.cbnCalculate.Click += new System.EventHandler(this.cbnCalculate_Click);
            // 
            // tbxNumber1
            // 
            this.tbxNumber1.Location = new System.Drawing.Point(193, 28);
            this.tbxNumber1.Name = "tbxNumber1";
            this.tbxNumber1.Size = new System.Drawing.Size(64, 20);
            this.tbxNumber1.TabIndex = 1;
            this.tbxNumber1.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // tbxNumber2
            // 
            this.tbxNumber2.Location = new System.Drawing.Point(193, 54);
            this.tbxNumber2.Name = "tbxNumber2";
            this.tbxNumber2.Size = new System.Drawing.Size(64, 20);
            this.tbxNumber2.TabIndex = 2;
            this.tbxNumber2.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // lblq1
            // 
            this.lblq1.AutoSize = true;
            this.lblq1.Location = new System.Drawing.Point(27, 28);
            this.lblq1.Name = "lblq1";
            this.lblq1.Size = new System.Drawing.Size(29, 13);
            this.lblq1.TabIndex = 3;
            this.lblq1.Text = "lblq1";
            // 
            // lblq2
            // 
            this.lblq2.AutoSize = true;
            this.lblq2.Location = new System.Drawing.Point(27, 54);
            this.lblq2.Name = "lblq2";
            this.lblq2.Size = new System.Drawing.Size(29, 13);
            this.lblq2.TabIndex = 3;
            this.lblq2.Text = "lblq2";
            // 
            // lbls1
            // 
            this.lbls1.AutoSize = true;
            this.lbls1.Location = new System.Drawing.Point(125, 89);
            this.lbls1.Name = "lbls1";
            this.lbls1.Size = new System.Drawing.Size(28, 13);
            this.lbls1.TabIndex = 4;
            this.lbls1.Text = "lbls1";
            // 
            // lblSubtotal
            // 
            this.lblSubtotal.AutoSize = true;
            this.lblSubtotal.Location = new System.Drawing.Point(190, 89);
            this.lblSubtotal.MinimumSize = new System.Drawing.Size(50, 0);
            this.lblSubtotal.Name = "lblSubtotal";
            this.lblSubtotal.Size = new System.Drawing.Size(50, 13);
            this.lblSubtotal.TabIndex = 5;
            this.lblSubtotal.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // lbls2
            // 
            this.lbls2.AutoSize = true;
            this.lbls2.Location = new System.Drawing.Point(125, 111);
            this.lbls2.Name = "lbls2";
            this.lbls2.Size = new System.Drawing.Size(28, 13);
            this.lbls2.TabIndex = 6;
            this.lbls2.Text = "lbls2";
            // 
            // lblGST
            // 
            this.lblGST.AutoSize = true;
            this.lblGST.Location = new System.Drawing.Point(190, 111);
            this.lblGST.MinimumSize = new System.Drawing.Size(50, 0);
            this.lblGST.Name = "lblGST";
            this.lblGST.Size = new System.Drawing.Size(50, 13);
            this.lblGST.TabIndex = 7;
            this.lblGST.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // lblTotal
            // 
            this.lblTotal.AutoSize = true;
            this.lblTotal.Location = new System.Drawing.Point(190, 146);
            this.lblTotal.MinimumSize = new System.Drawing.Size(50, 0);
            this.lblTotal.Name = "lblTotal";
            this.lblTotal.Size = new System.Drawing.Size(50, 13);
            this.lblTotal.TabIndex = 9;
            this.lblTotal.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // lbls3
            // 
            this.lbls3.AutoSize = true;
            this.lbls3.Location = new System.Drawing.Point(125, 146);
            this.lbls3.Name = "lbls3";
            this.lbls3.Size = new System.Drawing.Size(28, 13);
            this.lbls3.TabIndex = 8;
            this.lbls3.Text = "lbls3";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(284, 261);
            this.Controls.Add(this.lblTotal);
            this.Controls.Add(this.lbls3);
            this.Controls.Add(this.lblGST);
            this.Controls.Add(this.lbls2);
            this.Controls.Add(this.lblSubtotal);
            this.Controls.Add(this.lbls1);
            this.Controls.Add(this.lblq2);
            this.Controls.Add(this.lblq1);
            this.Controls.Add(this.tbxNumber2);
            this.Controls.Add(this.tbxNumber1);
            this.Controls.Add(this.cbnCalculate);
            this.Name = "Form1";
            this.Text = "GST Calculator";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button cbnCalculate;
        private System.Windows.Forms.TextBox tbxNumber1;
        private System.Windows.Forms.TextBox tbxNumber2;
        private System.Windows.Forms.Label lblq1;
        private System.Windows.Forms.Label lblq2;
        private System.Windows.Forms.Label lbls1;
        private System.Windows.Forms.Label lblSubtotal;
        private System.Windows.Forms.Label lbls2;
        private System.Windows.Forms.Label lblGST;
        private System.Windows.Forms.Label lblTotal;
        private System.Windows.Forms.Label lbls3;
    }
}

