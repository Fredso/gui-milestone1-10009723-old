﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

// The Blank Page item template is documented at http://go.microsoft.com/fwlink/?LinkId=402352&clcid=0x409

namespace Task_02_uwp
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class MainPage : Page
    {
        /********** UI LOGIC **********/
        public MainPage()
        {
            this.InitializeComponent();
            lblq1.Text = q1;
            lblq2.Text = q2;
            lbls1.Text = s1;
            lbls2.Text = s2;
            lbls3.Text = s3;
        }

        private void cbnCalculate_Click(object sender, RoutedEventArgs e)
        {
            var results = new string[3] { "", "", "" };
            results = Calculate(tbxNumber1.Text, tbxNumber2.Text);

            lblSubtotal.Text = results[0];
            lblGST.Text = results[1];
            lblTotal.Text = results[2];
        }

        /********** PROGRAM LOGIC **********/

        static string q1 = "Please introduce number 1:";
        static string q2 = "Please introduce number 2:";
        static string s1 = "Subtotal:";
        static string s2 = "GST:";
        static string s3 = "Total:";

        //It calculates the results
        static string[] Calculate(string N1, string N2)
        {
            var solutions = new string[3] { "", "", "" };
            double ND1, ND2;

            //It checks the introduced numbers
            bool result1 = double.TryParse(N1, out ND1);
            bool result2 = double.TryParse(N2, out ND2);
            if (result1 & result2)
            {
                solutions[0] = (ND1 + ND2).ToString("0.00");
                solutions[1] = ((ND1 + ND2) * 0.15).ToString("0.00");
                solutions[2] = ((ND1 + ND2) * 1.15).ToString("0.00");
            }
            else
            {
                solutions[0] = "Data not correct";
                solutions[1] = "Data not correct";
                solutions[2] = "Data not correct";
            }
            return solutions;
        }
    }
}
