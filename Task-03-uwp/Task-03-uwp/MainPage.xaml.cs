﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

// The Blank Page item template is documented at http://go.microsoft.com/fwlink/?LinkId=402352&clcid=0x409

namespace Task_03_uwp
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class MainPage : Page
    {
        /********** UI LOGIC **********/
        public MainPage()
        {
            this.InitializeComponent();
            lblq1.Text = q1;
            cbFruits.ItemsSource = Fruits;
        }

        private void cbxFruitSelection_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            lblSelection.Text = DisplaySelection(cbFruits.SelectedIndex.ToString());
        }

        /********** PROGRAM LOGIC **********/

        static string q1 = "Please select a fruit:";
        static string[] Fruits = new string[4] { "apple", "pear", "banana", "feijoa" };

        static string DisplaySelection(string selectiontxt)
        {
            int selection;
            var answer = "";
            bool result = int.TryParse(selectiontxt, out selection);
            if (result & selection < Fruits.GetLength(0))
            {
                answer = $"The fruit selected is {Fruits[selection]}.";
            }
            else
            {
                answer = "That is not a valid option. Try again next time!";
            }
            return answer;
        }
    }
}
