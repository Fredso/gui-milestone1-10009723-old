﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;



namespace Task_05_wf
{

    public partial class Form1 : Form
    {
        /********** UI LOGIC **********/
        public Form1()
        {
            InitializeComponent();
            lblq1.Text = Result.q1;
            lblq2.Text = Result.q2;
            lblq3.Text = Result.q3;
            lblq4.Text = Result.q4;
            lblScore.Text = "0";
            lblRounds.Text = "0";
        }

        private void btnTryNumber_Click(object sender, EventArgs e)
        {
            Result result = new Result();
            result.CheckNumber(tbxNumber.Text);
            lblMessage.Text = Result.Rmessage;
            lblScore.Text = Result.Score.ToString();
            lblRounds.Text= Result.Rounds.ToString();
        }
    }

    /********** PROGRAM LOGIC **********/

    class Result
    {
        public static int Rounds = 0;
        public static int Score = 0;
        public static string Rmessage;
        public static string q1 = "Try to discover the number. You have 5 attempts:";
        public static string q2 = "Your rounds:";
        public static string q3 = "Your Score:";
        public static string q4 = "Please, introduce a number from 1 to 5:";

        //Method to see if the introduced number is the same as the random one
        public void CheckNumber(string txtnumber)
        {
            int number;
            Random rnd = new Random();
            int rnumber = rnd.Next(1, 6);

            //It checks how many rounds has the user played
            Rounds++;
            if (Rounds <= 5)
            {
                //It checks the introduced numbers
                bool conversion = int.TryParse(txtnumber, out number);
                if (conversion&number>0&number<6)
                {
                    //It checks if the number is right
                    if (number == rnumber)
                    {
                        Score++;
                        Rmessage = $"You did it! {rnumber} was the number.";
                    }
                    else
                    {
                        //result.Score = 1;
                        Rmessage = $"Nope. The number was {rnumber} Try again!";
                    }
                }
                else
                {
                    //result.Score = 1;
                    Rmessage = "Please, enter a proper number next time.";
                }
            }
            else
            {
                Rounds = 5;
                Rmessage = "Sorry, you reached 5 attempts." +'\n'+ 
                    "You cannot continue playing.";
            }
        }
    }

}
